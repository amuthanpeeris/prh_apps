﻿using System;
using System.Collections.Generic;
using NRules.Fluent;
using NRules.Fluent.Dsl;
using PRH.Azure.Common.Rules.Plugins;

namespace PRH.Azure.Common.Rules.Runtime;

public class RuleActivator : IRuleActivator
{
    private readonly Context _context;

    public RuleActivator(Context context)
    {
        _context = context;
    }

    public IEnumerable<Rule> Activate(Type type)
    {
        yield return (Rule)System.Activator.CreateInstance(type,args: _context);
    }
}
