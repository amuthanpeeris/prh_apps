﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Azure.Identity;
using Microsoft.Extensions.Azure;

namespace PRH.Azure.Common.Rules.Runtime
{
    public class Program
    {
        public static async Task Main()
        {
            var host = new HostBuilder()
                .ConfigureAppConfiguration(builder =>
                {
                    string cs = Environment.GetEnvironmentVariable("appConfigEndpoint");
                    builder.AddAzureAppConfiguration(c =>
                    {
                        c.Connect(new Uri(cs), new DefaultAzureCredential());
                    });
                })
                .ConfigureServices(s =>
                {
                    s.AddLogging();
                    s.AddTransient<IMapperRuleHost, MapperRuleHost>();

                })
                .ConfigureFunctionsWorkerDefaults()
                .Build();

            await host.RunAsync();
        }
    }
}
