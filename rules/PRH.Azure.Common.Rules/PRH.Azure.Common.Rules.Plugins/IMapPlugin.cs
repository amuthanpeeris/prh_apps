﻿namespace PRH.Azure.Common.Rules.Plugins
{
    public interface IMapPlugin
    {
        public Type SourceMessageType { get; }
        public Type TargetMessageType { get; }
        public string MapName { get; }

        public IEnumerable<string> ConfigTokens { get; }

    }
}
