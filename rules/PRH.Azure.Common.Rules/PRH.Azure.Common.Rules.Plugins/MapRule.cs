﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NRules.Fluent.Dsl;

namespace PRH.Azure.Common.Rules.Plugins
{
    public class Context
    {
        public Dictionary<string, string> Tokens{ get; set; }
        public IConfiguration Configuration { get; set; }
    }

    public abstract class MapRule<TS, TT> : Rule, IMapPlugin
    {
        protected readonly Context Context;

        public MapRule() {}

        public MapRule(Context context)
        {
            Context = context;
        }
        public Type SourceMessageType => typeof(TS);
        public Type TargetMessageType => typeof(TT);
        public override void Define()
        {

            TS source = default;
            TT target = default;
            When()
                .Match<TS>(() => source)
                .Match<TT>(() => target);
            Then()
                .Do(ctx => Map(source, target));
        }

        public abstract void Map(TS source, TT target);
        public abstract string MapName { get; }

        public virtual IEnumerable<string> ConfigTokens => new string[] { MapName };

    }
}
