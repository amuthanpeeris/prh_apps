﻿using System.Runtime.CompilerServices;

namespace PRH.Azure.Common.Rules.SampleMaps.SimpleMap
{
    public class OutputMessage
    {
        public string? OutputValue { get; set; }
    }

}
