﻿using PRH.Azure.Common.Rules.Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRH.Azure.Common.Rules.SampleMaps.SimpleMap
{
    [Export(typeof(IMapPlugin))]
    public class SimpleMap: MapRule<InputMessage, OutputMessage>
    {
        /// <summary>
        /// Default constructor required.
        /// </summary>
        public SimpleMap() { }

        /// <summary>
        /// Context Constructor required
        /// </summary>
        /// <param name="context"></param>
        public SimpleMap(Context context) : base(context)
        {
        }

        public override void Map(InputMessage source, OutputMessage target)
        {
            // This key provided from Context.Tokens (defined in ConfigTokens)
            var configKey1 = Context.Tokens["Test:Key1"];

            // Direct access to IConfiguration if required
            var configKey2 = Context.Configuration["Test:Key1"];

            target.OutputValue = $"{source.Id}-{source.InputValue}-{configKey1}-{configKey2}";

        }

        public override string MapName => "SampleMaps.SimpleMap";

        public override IEnumerable<string> ConfigTokens => new[] { "Map1:Test:Nothere", "Test:Key1" };


    }
}
