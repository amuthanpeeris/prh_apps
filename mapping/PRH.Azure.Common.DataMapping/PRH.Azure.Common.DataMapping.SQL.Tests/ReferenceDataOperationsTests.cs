using FluentAssertions;
using System.Threading.Tasks;
using Xunit;

using PRH.Azure.Common.DataMapping.Common.Models;

namespace PRH.Azure.Common.DataMapping.SQL.Tests
{
    /// <summary>
    /// Test covering database operations (requires database deployment)
    /// </summary>
    public class ReferenceDataOperationsTests
    {
        private readonly string connectionString = "Server=localhost;Database=ReferenceDataStore;Trusted_Connection=True;";
        /// <summary>
        /// Test to see if LookUpReferenceData method works as expected when source data provided
        /// and target data available in the database
        /// </summary>
        [Fact]
        public async Task GivenSourceDataValue1_WhenDataAvailable_ThenShouldReturnTargetData()
        {
            // Arrange
            ReferenceData referenceData = new()
            {
                SourceSystem = "B3",
                TargetSystem = "USSAP",
                SourceFieldName = "Contributor Role",
                SourceDataValue1 = "Creator"
            };

            // Act
            ReferenceDataOperations referenceDataOperations = new();
            var retValue = await referenceDataOperations.LookUpReferenceDataAsync(referenceData, connectionString);

            // Assert
            retValue.Should().Be("M");
        }

        /// <summary>
        /// Test to see if LookUpReferenceData method works as expected when source data provided
        /// and target data available in the database
        /// </summary>
        [Fact]
        public async Task GivenSourceDataValue1and2_WhenDataAvailable_ThenShouldReturnTargetData()
        {
            // Arrange
            ReferenceData referenceData = new()
            {
                SourceSystem = "B3",
                TargetSystem = "AEM",
                SourceFieldName = "Product Type",
                SourceDataValue1 = "BZ",
                SourceDataValue2 = "B209"
            };

            // Act
            ReferenceDataOperations referenceDataOperations = new();
            var retValue = await referenceDataOperations.LookUpReferenceDataAsync(referenceData, connectionString);

            // Assert
            retValue.Should().Be("Sticker Book");
        }
    }
}
