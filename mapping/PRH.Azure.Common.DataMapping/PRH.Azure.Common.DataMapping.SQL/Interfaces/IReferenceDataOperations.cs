﻿using System.Threading.Tasks;

using PRH.Azure.Common.DataMapping.Common.Models;

namespace PRH.Azure.Common.DataMapping.SQL.Interfaces
{
    // Interfaces of Reference Data Operations
    public interface IReferenceDataOperations
    {
        string LookUpReferenceData(ReferenceData referenceData, string sqlConnString);
        Task<string> LookUpReferenceDataAsync(ReferenceData referenceData, string sqlConnString);
    }
}
