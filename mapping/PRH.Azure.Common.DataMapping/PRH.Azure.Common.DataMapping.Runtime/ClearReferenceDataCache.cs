using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Net;
using System.Threading.Tasks;

using PRH.Azure.Common.DataMapping.Redis.Interfaces;
using Microsoft.Extensions.Options;

namespace PRH.Azure.Common.DataMapping.Runtime
{
    // Clear Cache using Azure Function
    public class ClearReferenceDataCache
    {
        private readonly IRedisCacheAccessor _redisCacheAccessor;
        private readonly IOptionsSnapshot<Configuration> _configuration;
        private readonly ILogger<ClearReferenceDataCache> _logger;

        public ClearReferenceDataCache(IRedisCacheAccessor redisCacheAccessor, IOptionsSnapshot<Configuration> configuration,
            ILogger<ClearReferenceDataCache> logger)
        {
            _redisCacheAccessor = redisCacheAccessor;
            _configuration = configuration;
            _logger = logger;
        }

        /// <summary>
        /// Function to clear redis chache
        /// </summary>
        /// <param name="req">Request can be empty</param>
        [FunctionName("ClearReferenceDataCache")]
        [OpenApiOperation(operationId: "Run", tags: new[] { "name" })]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiParameter(name: "name", In = ParameterLocation.Query, Required = true, Type = typeof(string), Description = "The **Name** parameter")]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "text/plain", bodyType: typeof(string), Description = "The OK response")]
        public async Task Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req)
        {
            // Read the configuration
            var cacheConnectionString = _configuration.Value.RedisCacheConnectionString;
            try
            {
                await _redisCacheAccessor.FlushDatabaseAsync(cacheConnectionString);
                _logger.LogInformation("Redis Cache Database cleared successfully");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception occurred while trying to clear cached data in Redis. Exception: {ex.Message}");
            }
        }
    }
}
