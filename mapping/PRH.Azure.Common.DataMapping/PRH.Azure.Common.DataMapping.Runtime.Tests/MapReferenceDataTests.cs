using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Xunit;

using PRH.Azure.Common.DataMapping.Common.Models;
using PRH.Azure.Common.DataMapping.Redis.Interfaces;
using PRH.Azure.Common.DataMapping.SQL.Interfaces;
using Microsoft.Extensions.Options;

namespace PRH.Azure.Common.DataMapping.Runtime.Tests
{
    /// <summary>
    /// Tests covering azure function implementation with database and redis cache
    /// </summary>
    public class MapReferenceDataTests
    {
        private readonly Mock<IRedisCacheAccessor> _redisCacheAccessor;
        private readonly Mock<IReferenceDataOperations> _referenceDataOperations;
        private readonly Mock<ILogger<MapReferenceData>> _logger;
        private readonly Mock<IOptionsSnapshot<Configuration>> _configuration;

        public MapReferenceDataTests()
        {
            _redisCacheAccessor = new Mock<IRedisCacheAccessor>();
            _referenceDataOperations = new Mock<IReferenceDataOperations>();
            _logger = new Mock<ILogger<MapReferenceData>>();
            _configuration = new Mock<IOptionsSnapshot<Configuration>>();
        }

        /// <summary>
        /// Test to check if the function runtime throws bad request exception when null request passed
        /// </summary>
        [Fact]
        public async Task Http_trigger_with_nullrequest_should_return_badrequesterror()
        {
            // Assign
            MapReferenceData mapReferenceData = new(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object, _configuration.Object, null);

            // Act
            var responseBody = await mapReferenceData.RunAsync(null) as ObjectResult;

            // Assert
            responseBody.Should().NotBeNull();
            responseBody?.StatusCode.Should().Be(400);
            responseBody?.Value.Should().Be("Invalid request or no requst body found");
        }

        /// <summary>
        /// Test to check if the function runtime throws bad request exception when empty request passed
        /// </summary>
        [Fact]
        public async Task Http_trigger_with_emptyrequest_should_return_badrequesterror()
        {
            // Assign
            MapReferenceData mapReferenceData = new(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object, _configuration.Object, null);
            var request = TestFactory.CreateHttpRequest("");

            // Act
            var responseBody = await mapReferenceData.RunAsync(request) as ObjectResult;

            // Assert
            responseBody.Should().NotBeNull();
            responseBody?.StatusCode.Should().Be(400);
            responseBody?.Value.Should().Be("Invalid request or no requst body found");
        }

        /// <summary>
        /// Test to check if the function runtime throws bad request exception when invalid request passed
        /// </summary>
        [Fact]
        public async Task Http_trigger_with_invalidrequest_should_return_badrequesterror()
        {
            // Assign
            MapReferenceData mapReferenceData = new(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object, _configuration.Object, null);
            var request = TestFactory.CreateHttpRequest("Invalid Request");

            // Act
            var responseBody = await mapReferenceData.RunAsync(request) as ObjectResult;

            // Assert
            responseBody.Should().NotBeNull();
            responseBody?.StatusCode.Should().Be(400);
            responseBody?.Value.Should().Be("Invalid request or no requst body found");
        }

        /// <summary>
        /// Test to check if the function runtime throws bad request exception when base64 request passed
        /// </summary>
        [Fact]
        public async Task Http_trigger_with_base64request_should_return_badrequesterror()
        {
            // Assign
            MapReferenceData mapReferenceData = new(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object, _configuration.Object, null);
            var request = TestFactory.CreateHttpRequest("SW52YWxpZCBSZXF1ZXN0");

            // Act
            var responseBody = await mapReferenceData.RunAsync(request) as ObjectResult;

            // Assert
            responseBody.Should().NotBeNull();
            responseBody?.StatusCode.Should().Be(400);
            responseBody?.Value.Should().Be("Invalid request or no requst body found");
        }

        /// <summary>
        /// Test to see if the azure function works as expected when a data is available in the cache
        /// </summary>
        [Fact]
        public async Task Http_trigger_should_return_targetdata_from_cache()
        {
            // Assign
            MapReferenceData mapReferenceData = new(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object, _configuration.Object, null);
            var request = TestFactory.CreateHttpRequest("[{\"sourceSystem\":\"B3\",\"targetSystem\":\"USSAP\",\"sourceFieldName\":\"Contributor Role\",\"SourceDataValue1\":\"External Designer\"}]");
            _redisCacheAccessor.Setup(x => x.GetCacheAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync("O");

            // Act
            var responseBody = await mapReferenceData.RunAsync(request) as ObjectResult;

            // Assert
            responseBody.Should().NotBeNull();
            responseBody?.Value?.Should().NotBeNull();
            var targetData = responseBody?.Value?.ToString();
            if (!string.IsNullOrWhiteSpace(targetData))
            {
                List<ReferenceData> ?data = JsonConvert.DeserializeObject<List<ReferenceData>>(targetData);
                data?[0].TargetData.Should().Be("O");
            }
        }

        /// <summary>
        /// Test to see if the azure function works as expected when a data is not available in the cache
        /// but available in the database
        /// </summary>
        [Fact]
        public async Task Http_trigger_should_return_targetdata_from_database()
        {
            // Assign
            MapReferenceData mapReferenceData = new(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object, _configuration.Object, null);
            var request = TestFactory.CreateHttpRequest("[{\"sourceSystem\":\"B3\",\"targetSystem\":\"USSAP\",\"sourceFieldName\":\"Contributor Role\",\"SourceDataValue1\":\"External Designer\"}]");
            _redisCacheAccessor.Setup(x => x.GetCacheAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(string.Empty);
            _referenceDataOperations.Setup(x => x.LookUpReferenceDataAsync(It.IsAny<ReferenceData>(), It.IsAny<string>())).ReturnsAsync("M");

            // Act
            var responseBody = await mapReferenceData.RunAsync(request) as ObjectResult;

            // Assert
            responseBody.Should().NotBeNull();
            responseBody?.Value.Should().NotBeNull();
            var targetData = responseBody?.Value?.ToString();
            if (!string.IsNullOrWhiteSpace(targetData))
            {
                List<ReferenceData>? data = JsonConvert.DeserializeObject<List<ReferenceData>>(targetData);
                data?[0].TargetData.Should().Be("M");
            }
        }

        /// <summary>
        /// Test to see if the azure function works as expected when a data is not available in both
        /// cache and database - should return blank target data
        /// </summary>
        [Fact]
        public async Task Http_trigger_should_return_blanktargetdata()
        {
            // Assign
            MapReferenceData mapReferenceData = new(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object, _configuration.Object, null);
            var request = TestFactory.CreateHttpRequest("[{\"sourceSystem\":\"B3\",\"targetSystem\":\"USSAP\",\"sourceFieldName\":\"Contributor Role\",\"SourceDataValue1\":\"External Designer\"}]");
            _redisCacheAccessor.Setup(x => x.GetCacheAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(string.Empty);
            _referenceDataOperations.Setup(x => x.LookUpReferenceDataAsync(It.IsAny<ReferenceData>(), It.IsAny<string>())).ReturnsAsync(string.Empty);

            // Act
            var responseBody = await mapReferenceData.RunAsync(request) as ObjectResult;

            // Assert
            responseBody.Should().NotBeNull();
            responseBody?.Value.Should().NotBeNull();
            var targetData = responseBody?.Value?.ToString();
            if (!string.IsNullOrWhiteSpace(targetData))
            {
                List<ReferenceData>? data = JsonConvert.DeserializeObject<List<ReferenceData>>(targetData);
                data?[0].TargetData.Should().BeEmpty();
            }
        }
    }
}
