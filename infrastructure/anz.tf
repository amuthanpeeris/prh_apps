module "data_map" {
  source                           = "./logic_app"
  name_prefix                      = var.storage_env
  name_suffix                      = "anz-${var.role}-${var.region}"
  stname_suffix                    = "anz${var.role}${var.region}"
  resource_group_name              = azurerm_resource_group.ais.name
  resource_group_location          = azurerm_resource_group.ais.location
  kind                             = var.storage_account_kind
  tier                             = var.storage_account_tier
  replication_type                 = var.storage_account_replication_type
  access_tier                      = var.storage_account_access_tier
  os_type                          = var.logic_app_service_plan_os
  service_plan_sku                 = var.logic_app_service_plan_sku
  tags                             = local.common_tags
}
