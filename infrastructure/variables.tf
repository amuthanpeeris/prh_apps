variable "location" {
  type        = string
  description = "Location of the resource group."
  default     = "UkSouth"
}

variable "role" {
  type        = string
  description = "Role of the resource"
  default     = "ais"
}

variable "region" {
  type        = string
  description = "Azure region the resource is located, this is the full region name e.g. UK South."
  default     = "uks"
}

variable "tenant_id" {
  type        = string
  description = "Unique identifier of azure active directory instance"
  default     = "dummy"
}

variable "company" {
  type        = string
  description = "Company Name for tagging"
  default     = "Penguin Random House"
}

variable "project" {
  type        = string
  description = "Project Name for tagging"
  default     = "PRH-IaC"
}

variable "billing_code" {
  type        = string
  description = "Billing Code for tagging"
  default     = 12345
}

variable "subscription_id" {
  type        = string
  description = "Unique identification of the Azure subscription"
  default     = "dummy"
}

variable "client_id" {
  default     = "dummy"
  description = "Client id generated from service principal"
  type        = string
}

variable "client_secret" {
  type        = string
  description = "client secret is the password of the service principal."
  default     = "dummy"
}

variable "Team" {
  type        = string
  description = "The team that this resource belongs to"
  default     = "Integration"
}

variable "BusinessUnit" {
  type        = string
  description = "Business Unit"
  default     = "Integration"
}

variable "BusinessUnitOwner" {
  type        = string
  description = "Business Unit Owner"
  default     = "Integration"
}

variable "ProjectCode" {
  type        = string
  description = "financecode"
  default     = "B0285"
}

variable "CostCentre" {
  type        = string
  description = "Costcentre"
  default     = "Integration"
}

variable "ApplicationId" {
  type        = string
  description = "Name of the application that this resource serves."
  default     = "Integration"
}

variable "ApplicationRole" {
  type        = string
  description = "The purpose this application serves."
  default     = "Integration"
}

variable "AppVersion" {
  type        = string
  description = "The version of the app"
  default     = "1.0.0"
}

variable "Compliance" {
  type        = string
  description = "Any compliance identifier that this resource needs to follow"
  default     = "PCI"
}

variable "Product" {
  type        = string
  description = "Identifies what product that this resource is part of"
  default     = "Integration"
}

variable "ProductOwner" {
  type        = string
  description = "Identify the product owner for this resource"
  default     = "Integration"
}

variable "ProjectOwnerContact" {
  type        = string
  description = "Email address of project owner for queries"
  default     = "Integration"
}

variable "ResourceOwnerContact" {
  type        = string
  description = "Email address of resource creator, if we want to automate deletion, this may be handy to notify."
  default     = "Integration"
}

variable "vnet_cidr_range" {
  type        = string
  description = "Virtual Network CIDR Range"
  default     = "10.0.0.0/16"
}

variable "subnet_names" {
  type        = list(string)
  description = "Name of the subnet"
  default     = ["web", "database"]
}

variable "apim_sku" {
  type        = string
  description = "APIM SKU Name"
  default     = "Developer_1"
}

variable "apim_publisher_name" {
  type        = string
  description = "APIM Publisher Name"
  default     = "Penguin Random House"
}

variable "white_list_ip" {
  type        = list(string)
  description = "List of IP Address to white-list"
  default     = ["106.220.73.215", "185.35.50.238", "92.237.85.230"]
}

variable "titlefeed_ref" {
  type        = string
  description = "Interface reference of Title Feed in the project"
  default     = "adianz00004a"
}

variable "pubeasy_ref" {
  type        = string
  description = "Interface reference of PubEasy in the project"
  default     = "pubeasy"
}

variable "storage_env" {
  type        = string
  description = "Environment - dev/test/prod etc"
  default     = "vin"
}

variable "log_analytics_sku" {
  type        = string
  description = "Log Analytics SKU"
  default     = "PerGB2018"
}

variable "app_configuration_sku" {
  type        = string
  description = "Application Configuration SKU"
  default     = "standard"
}

variable "redis_cache_sku" {
  type        = string
  description = "Redis Cache SKU"
  default     = "Premium"
}

variable "redis_cache_family" {
  type        = string
  description = "Redis Cache family"
  default     = "P"
}

variable "redis_cache_patch_schedule" {
  type        = string
  description = "Redis Cache Patch Scedule Day"
  default     = "Sunday"
}

variable "keyvault_sku" {
  type        = string
  description = "Key Vault SKU"
  default     = "premium"
}

variable "storage_account_kind" {
  type        = string
  description = "Storage Account Kind"
  default     = "StorageV2"
}

variable "storage_account_tier" {
  type        = string
  description = "Storage Account Tier"
  default     = "Standard"
}

variable "storage_account_replication_type" {
  type        = string
  description = "Storage Account Replication Type"
  default     = "GRS"
}

variable "storage_account_access_tier" {
  type        = string
  description = "Storage Account Access Tier"
  default     = "Hot"
}

variable "func_app_service_plan_os" {
  type        = string
  description = "App Service Plan OS"
  default     = "Windows"
}

variable "func_app_service_plan_sku" {
  type        = string
  description = "App Service Plan SKU"
  default     = "EP1"
}

variable "logic_app_service_plan_os" {
  type        = string
  description = "App Service Plan OS"
  default     = "Windows"
}

variable "logic_app_service_plan_sku" {
  type        = string
  description = "App Service Plan SKU"
  default     = "WS1"
}

variable "vnet_address_space" {
  type        = list(string)
  description = "Virtual Network Address"
  default     = ["10.0.0.0/16"]
}

variable "snet_kv_address_prefixes" {
  type        = list(string)
  description = "Key Vault Subnet Address"
  default     = ["10.0.0.0/24"]
}

variable "snet_sql_address_prefixes" {
  type        = list(string)
  description = "SQL Subnet Address"
  default     = ["10.0.3.0/24"]
}

variable "snet_sql_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "SQL Private Endpoint Subnet Address"
  default     = ["10.0.4.0/24"]
}

variable "snet_datamap_address_prefixes" {
  type        = list(string)
  description = "DataMap function app Subnet Address"
  default     = ["10.0.1.0/24"]
}

variable "snet_datamap_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "DataMap function app Private Endpoint Subnet Address"
  default     = ["10.0.11.0/24"]
}

variable "snet_rulesmap_address_prefixes" {
  type        = list(string)
  description = "RulesMap function app Subnet Address"
  default     = ["10.0.2.0/24"]
}

variable "snet_rulesmap_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "RulesMap function app Private Endpoint Subnet Address"
  default     = ["10.0.10.0/24"]
}

variable "snet_anz_logicapp_address_prefixes" {
  type        = list(string)
  description = "ANZ logic app Subnet Address"
  default     = ["10.0.5.0/24"]
}

variable "snet_anz_logicapp_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "ANZ logic app Private Endpoint Subnet Address"
  default     = ["10.0.10.0/24"]
}

variable "snet_pubeasy_logicapp_address_prefixes" {
  type        = list(string)
  description = "Pubeasy logic app Subnet Address"
  default     = ["10.0.7.0/24"]
}

variable "snet_pubeasy_logicapp_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "Pubeasy logic app Private Endpoint Subnet Address"
  default     = ["10.0.8.0/24"]
}

variable "snet_apim_address_prefixes" {
  type        = list(string)
  description = "APIM Subnet Address"
  default     = ["10.0.6.0/24"]
}

variable "ussap_ref" {
  type        = string
  description = "Interface reference of US SAP in the project"
  default     = "bib-ussap"
}

variable "snet_ussaptitle_logicapp_address_prefixes" {
  type        = list(string)
  description = "ussaptitle logic app Subnet Address"
  default     = ["10.0.12.0/24"]
}

variable "snet_ussaptitle_logicapp_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "ussaptitle logic app Private Endpoint Subnet Address"
  default     = ["10.0.13.0/24"]
}

variable "snet_ussappipo_logicapp_address_prefixes" {
  type        = list(string)
  description = "ussappipo logic app Subnet Address"
  default     = ["10.0.14.0/24"]
}

variable "snet_ussappipo_logicapp_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "ussappipo logic app Private Endpoint Subnet Address"
  default     = ["10.0.15.0/24"]
}

variable "snet_ussap_functionapp_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "ussap logic app Private Endpoint Subnet Address"
  default     = ["10.0.16.0/24"]
}

variable "snet_ussap_logicapp_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "ussap logic app Private Endpoint Subnet Address"
  default     = ["10.0.17.0/24"]
}

variable "snet_appconfig_private_endpoint_address_prefixes" {
  type        = list(string)
  description = "App Config Private Endpoint Subnet Address"
  default     = ["10.0.18.0/24"]
}

variable "snet_redis_address_prefixes" {
  type        = list(string)
  description = "Key Vault Subnet Address"
  default     = ["10.0.19.0/24"]
}
