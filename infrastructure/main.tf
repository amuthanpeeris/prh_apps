data "azurerm_client_config" "current" {}
data "azurerm_subscription" "primary" {}

resource "azurerm_resource_group" "ais" {
  location = var.location
  name     = "${local.environment}-rg-${var.role}-${var.region}"
}
