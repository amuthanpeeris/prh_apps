variable "name_prefix" {
  type        = string
  description = "resource name prefix"
}

variable "name_suffix" {
  type        = string
  description = "resource name suffix"
}

variable "resource_group_name" {
  type        = string
  description = "resource group name"
}

variable "resource_group_location" {
  type        = string
  description = "resource group location"
}

variable "kind" {
  type        = string
  description = "kind"
}

variable "tier" {
  type        = string
  description = "kind"
}

variable "replication_type" {
  type        = string
  description = "kind"
}

variable "access_tier" {
  type        = string
  description = "kind"
}

variable "tags" {
  type        = map
  description = "tags"    
}
