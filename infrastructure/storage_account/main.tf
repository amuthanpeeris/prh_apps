resource "azurerm_storage_account" "ais_storage" {
  #checkov:skip=CKV2_AZURE_33:Clashes with CKV_AZURE_35
  name                          = "${var.name_prefix}st${var.name_suffix}"
  resource_group_name           = var.resource_group_name
  location                      = var.resource_group_location
  account_kind                  = var.kind
  account_tier                  = var.tier
  account_replication_type      = var.replication_type
  access_tier                   = var.access_tier
  enable_https_traffic_only     = true
  is_hns_enabled                = false
  min_tls_version               = "TLS1_2"
  shared_access_key_enabled     = true  
  public_network_access_enabled = true
  #checkov:skip=CKV_AZURE_59:Allow public access for deployment
  allow_nested_items_to_be_public = false
  queue_properties {
    logging {
      delete                = true
      read                  = true
      write                 = true
      version               = "1.0"
      retention_policy_days = 10
    }
  }
  identity {
    type = "SystemAssigned"
  }
  tags = var.tags
}
