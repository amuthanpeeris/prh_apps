output "id" {
    value = azurerm_storage_account.ais_storage.id
}

output "name" {
    value = azurerm_storage_account.ais_storage.name
}

output "primary_access_key" {
    value = azurerm_storage_account.ais_storage.primary_access_key
}

output "principal_id" {
    value = azurerm_storage_account.ais_storage.identity[0].principal_id
}

output "primary_blob_endpoint" {
    value = azurerm_storage_account.ais_storage.primary_blob_endpoint
}
