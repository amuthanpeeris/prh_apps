module "storage_account" {
  source                           = "../storage_account"
  name_prefix                      = var.name_prefix
  name_suffix                      = var.stname_suffix
  resource_group_name              = var.resource_group_name
  resource_group_location          = var.resource_group_location
  kind                             = var.kind
  tier                             = var.tier
  replication_type                 = var.replication_type
  access_tier                      = var.access_tier
  tags                             = var.tags
}

resource "azurerm_service_plan" "ais" {
  name                = "${var.name_prefix}-asp-${var.name_suffix}"
  resource_group_name = var.resource_group_name
  location            = var.resource_group_location
  os_type             = var.os_type
  sku_name            = var.service_plan_sku
  worker_count        = 2
  tags                = var.tags
}

resource "azurerm_logic_app_standard" "ais" {
  name                       = "${local.environment}-logic-${local.logicapp_ref}-${var.region}"
  resource_group_name        = var.resource_group_name
  location                   = var.resource_group_location
  storage_account_name       = module.storage_account.name
  storage_account_access_key = module.storage_account.primary_access_key
  service_plan_id            = azurerm_service_plan.ais.id
  app_settings = {
    "FUNCTIONS_WORKER_RUNTIME"       = "node"
    "FUNCTIONS_EXTENSION_VERSION"    = "~4"
    "WEBSITE_NODE_DEFAULT_VERSION"   = "~18"
  }
  identity {
    type = "SystemAssigned"
  }
  tags = var.tags
}
